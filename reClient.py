import openpyxl
import numpy as np
import pandas as pd
from Filter import filter
from Search import sortirovkaMonth,printAfterCheck,checkLastTodayIntermediatMonth

book = openpyxl.load_workbook('TopLaser.xlsx')
# получаем список всех листов
sheets = book.sheetnames
# указываем какой столбик нас интересует
stolbik = 'I'
# создаем список(массив для)номеров
listTelNumber = []
#создаем список для названия месяцов
listMonth = []
#месяц пронумерованы (0:первый(сегодняшний), 2:последний)
for sheet in sheets:
    listMonth.append(sheet)
    listTelNumber.append(filter(book[sheet],stolbik))
print('Анализ')
print(listMonth)

sortirovkaMonth(listTelNumber)

checkLastTodayIntermediatMonth()

printAfterCheck()
