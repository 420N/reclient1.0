import openpyxl
import pandas as pd
import re

def filter (sheet,stolbik):
    # создаем список(массив для)номеров
    ListTelNumber = []

    for cell in sheet[stolbik]:
        if cell.value != None:
            if type(cell.value) == str:
                if re.search(r'\d+',cell.value) != None :
                    digit = ''.join(x for x in cell.value if x.isdigit())
                    ListTelNumber.append(digit)
            else:#если не стринг то берем его и переводим в стринг что бы массив был одного типа
                ListTelNumber.append(str(cell.value))
    return ListTelNumber
    
